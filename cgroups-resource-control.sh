#!/bin/sh
# vim: set sts=4 sw=4 et tw=0 :

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}"/common/inherit-config.sh
. "${TESTDIR}"/common/update-test-path

CGFS_PATH="/sys/fs/cgroup"
MID_CG="middleware"
APP_CG="applications"

# Memory limits
MID_HARD_MEM_LIMIT=$((50*1024*1024))
APP_HARD_MEM_LIMIT=$((80*1024*1024))
# Used in _balloon_memory()
MAX_MEMORY_LIMIT=$((100*1024*1024))

# Running HTTP servers used for network cgroup test
HTTP_SERVER_PIDS=

# List of processes to kill on exit
PROC_KILL_LIST=

# Kill all pids in the list. It's okay if the list has stale pids.
# Theoretically, OSes recycle pids, but that's over a long period of time.
# We're unlikely to have collisions in the (relatively) short span of this test.
_kill_procs() {
    local proc
    echo "Killing processes: ${PROC_KILL_LIST}..."
    for proc in ${PROC_KILL_LIST}; do
        echo -n "Killing $proc... "
        if ps -p $proc >/dev/null; then
            kill -9 $proc
            echo "Done!"
        else
            echo "Not needed."
        fi
    done
    echo "Finished killing processes."
}

# Convenience functions
_drop_caches() {
    # Drop caches!
    echo "# Syncing disk..."
    sync
    echo "# Dropping I/O caches..."
    echo 3 > /proc/sys/vm/drop_caches
}

# Run cpuburn for the specified amount of time
_timed_cpuburn() {
    echo "# $$: Starting ${CPUBURN}..."
    "${CPUBURN}" &
    PROC_KILL_LIST="${PROC_KILL_LIST} $!"
    echo "# $$: (process $!)"
    echo "# $$: Waiting $1 seconds with ${CPUBURN} running..."
    sleep $1
    echo "# $$: Killing ${CPUBURN}..."
    kill $PROC_KILL_LIST
}

# Use up all available CPUs
_burn_cpus() {
    local lastpid
    local nsecs=15
    local ncpus

    ncpus=$(grep ^processor /proc/cpuinfo | wc -l)
    while [ ${ncpus} -gt 0 ]; do
        _timed_cpuburn ${nsecs} &
        PROC_KILL_LIST="${PROC_KILL_LIST} $!"
        ncpus=$((${ncpus} - 1))
    done
    wait
}

# Use up memory till we hit the cgroup limit
# TODO: Replace this with a C program that does read() on urandom in chunks.
#       That would be more predictable, and less prone to breakage.
_balloon_memory() {
    local data
    local total=0
    local quantum=$((1*1024*1024))
    local logfile="${1}"
    echo "# $$: Eating ${MAX_MEMORY_LIMIT} bytes in ${quantum} byte increments..."
    # Eat memory up to MAX_MEMORY_LIMIT, so that we don't fill up all the RAM
    # if the cgroup limits aren't working
    while [ $total -lt ${MAX_MEMORY_LIMIT} ]; do
        echo -n .
        total=$((${total}+${quantum}))
        echo $total > "${logfile}"
        data="$data$(dd if=/dev/urandom bs=$quantum count=1 2>/dev/null)"
    done
    echo "# $$: stopped after >= ${MAX_MEMORY_LIMIT} bytes"
}

# Reset the given cgroup
_reset_cgroup() {
    local i
    local cg
    local cg_type=$1
    echo "Resetting cgroup $cg_type"
    for i in "$MID_CG" "$APP_CG"; do
        cg="${CGFS_PATH}/${cg_type}/$i"
        # Reset cgroup -- rmdir is the trigger for removing a cgroup
        if [ -d "$cg" ]; then
          echo "# Removing $cg..."
          rmdir "$cg"
        fi
        echo "# Creating $cg..."
        mkdir -p "$cg"
    done
}

# Write out "$1" megabyte files filled with urandom data
_write_random_files() {
    # We don't create these in the temporary WORKDIR because it's easy for them 
    # to get left behind if the test is interrupted and that will cause the disk
    # to get filled up. By using BASEWORKDIR we ensure that they will get reused
    # and eventually deleted upon a complete test run.
    mid_test_file="${BASEWORKDIR}/${MID_CG}_random_data.bin"
    app_test_file="${BASEWORKDIR}/${APP_CG}_random_data.bin"

    # Write files (in parallel because it's CPU bound)
    echo "# Writing random $1 MiB file to \"$mid_test_file\"..."
    dd if=/dev/urandom of="${mid_test_file}" bs=1M count=$1 &
    PROC_KILL_LIST="${PROC_KILL_LIST} $!"
    echo "# (process $!)"
    echo "# Writing random $1 MiB file to \"$app_test_file\"..."
    dd if=/dev/urandom of="${app_test_file}" bs=1M count=$1 &
    PROC_KILL_LIST="${PROC_KILL_LIST} $!"
    echo "# (process $!)"
    echo "# Waiting for I/O processes to complete..."
    wait
    echo "# ... done."
}

# Setup for blkio tests
_setup_blkio() {
    root_partition="$(grep ' / ' /proc/self/mounts | cut -d' ' -f1 | grep -v '^rootfs$')"
    root_drive="$(echo "${root_partition}" | sed -e 's,p\?[0-9]\+$,,')"
    root_drive="${root_drive#/dev/}"
    head -v /sys/block/${root_drive}/queue/scheduler 2>&1 | sed -e 's/^/# /'
    case "$(cat /sys/block/${root_drive}/queue/scheduler)" in
        (*\[cfq\]*)
            ;;
        (*)
            echo "WARNING: Block device scheduler does not appear to be cfq."
            echo "This is probably not going to work, but trying it anyway..."
    esac

    _reset_cgroup blkio
    # Set the resource allocation proportion
    echo "# Configuring $MID_CG cgroup with blkio.weight = 1000"
    echo 1000 > "${CGFS_PATH}/blkio/${MID_CG}/blkio.weight"
    echo "# Configuring $APP_CG cgroup with blkio.weight = 10"
    echo 10   > "${CGFS_PATH}/blkio/${APP_CG}/blkio.weight"
    _write_random_files 200
    _drop_caches
}

_do_random_read() {
    local cmd="random-reads"
    echo "# $$: ${cmd} ${1}..."
    ${cmd} "${1}"
    echo "# $$: done"
}

# Run, log, and save exit status at the end of the log file
_run_memory_threshold_notifier() {
    local logfile=$1
    local cg=$2
    local cmd="cgroup-memory-threshold-notifier"

    echo "# $$: ${TESTLIBDIR}/${cmd} ${cg} 10M..."
    "${TESTLIBDIR}/${cmd}" "${cg}" 10M > "${logfile}" &
    PROC_KILL_LIST="${PROC_KILL_LIST} $!"
    echo "# $$: (process $!)"
    wait %1
    e=$?
    echo "# $$: process $! finished, exit status $e"
    sed -e "s,^,# $$/$!: ," < "${logfile}"
    echo $e >> "${logfile}"
}

_setup_netprio() {
    local cg_type="net_prio"

    mkdir -p "${CGFS_PATH}/${cg_type}/${APP_CG}"
    mkdir -p "${CGFS_PATH}/${cg_type}/${MID_CG}"
    # Give the applications cgroup a low priority (10:2)
    echo "lo 0" > "${CGFS_PATH}/${cg_type}/${APP_CG}/net_prio.ifpriomap"
    # Give the middleware cgroup a high priority (10:1 and 10:2)
    echo "lo 7" > "${CGFS_PATH}/${cg_type}/${MID_CG}/net_prio.ifpriomap"
}

_setup_qdiscs() {
    # Delete any old stuff, if needed
    _wipe_qdiscs || true
    # Rate-limit loopback so we have streams competing for the bandwidth
    tc qdisc add dev lo root handle 1: netem rate 90mbit
    # Setup a standard three-class PRIO scheduler
    # Bulk traffic will go to 10:3, interactive traffic to 10:2 or 10:1
    tc qdisc add dev lo parent 1:1 handle 10: prio
    # Setup leaf nodes
    tc qdisc add dev lo parent 10:1 sfq
    tc qdisc add dev lo parent 10:2 sfq
    tc qdisc add dev lo parent 10:3 sfq
}

_setup_http_server() {
    local pid
    local port=$1
    local cg_path="$2"

    cd "${BASEWORKDIR}"
    echo "# Starting HTTP server on port $port..."
    python3 -m http.server ${port} > "${WORKDIR}/network-cgroup-http-server-${port}.log" 2>&1 &
    pid=$!
    echo "# (putting HTTP server process $! in ${cg_path} cgroup...)"
    echo $pid > "${CGFS_PATH}/${cg_path}/tasks"
    HTTP_SERVER_PIDS="$HTTP_SERVER_PIDS $pid"
    PROC_KILL_LIST="${PROC_KILL_LIST} $(cat "${CGFS_PATH}/${cg_path}/tasks")"
}

_wipe_qdiscs() {
    tc qdisc delete dev lo root
}

#########
# Setup #
#########
trap "setup_failure" EXIT

# CPU Burn executable
if [ -n "${CPUBURN}" ]; then
    :
elif which burnBX >/dev/null; then
    CPUBURN="$(which burnBX)"
elif which burnCortexA9 >/dev/null; then
    CPUBURN="$(which burnCortexA9)"
else
    cry "Unable to find cpuburn!"
    setup_failure
fi

# Curl executable
if [ -n "${CURL}" ]; then
    :
elif which curl >/dev/null; then
    CURL="$(which curl)"
else
    cry "Unable to find curl!"
    setup_failure
fi

# Need root to setup and use cgroups
check_have_root

setup_success

###########
# Execute #
###########
test_blkio_weights_sequential_read() {
    local mid_test_file
    local app_test_file
    local ret=0
    _setup_blkio

    ## Test ##
    # Do sequential read disk I/O
    echo "# dd if=${mid_test_file} of=/dev/null..."
    dd if=${mid_test_file} of=/dev/null &
    echo "# (putting dd process $! in ${MID_CG} cgroup)"
    echo $! > "${CGFS_PATH}/blkio/${MID_CG}/tasks"
    PROC_KILL_LIST="${PROC_KILL_LIST} $(cat "${CGFS_PATH}/blkio/${MID_CG}/tasks")"

    echo "# dd if=${app_test_file} of=/dev/null..."
    dd if=${app_test_file} of=/dev/null &
    echo "# (putting dd process $! in ${APP_CG} cgroup)"
    echo $! > "${CGFS_PATH}/blkio/${APP_CG}/tasks"
    PROC_KILL_LIST="${PROC_KILL_LIST} $(cat "${CGFS_PATH}/blkio/${APP_CG}/tasks")"

    echo "# Waiting for sequential read processes..."
    wait

    head "${CGFS_PATH}/blkio/$APP_CG"/* 2>&1 | sed -e 's/^/# /'
    head "${CGFS_PATH}/blkio/$MID_CG"/* 2>&1 | sed -e 's/^/# /'

    ## Verify ##
    echo "# blkio.io_wait_time for $MID_CG cgroup:"
    sed -e 's/^/#   /' < ${CGFS_PATH}/blkio/$MID_CG/blkio.io_wait_time
    echo "# blkio.io_wait_time for $APP_CG cgroup:"
    sed -e 's/^/#   /' < ${CGFS_PATH}/blkio/$APP_CG/blkio.io_wait_time
    local mid_io_wait_time=$(grep ^Total ${CGFS_PATH}/blkio/$MID_CG/blkio.io_wait_time | cut -f2 -d\ )
    echo "# total wait time for $MID_CG cgroup: $mid_io_wait_time"
    local app_io_wait_time=$(grep ^Total ${CGFS_PATH}/blkio/$APP_CG/blkio.io_wait_time | cut -f2 -d\ )
    echo "# total wait time for $APP_CG cgroup: $app_io_wait_time"
    # Check that the applications CG got less I/O time than the middleware CG
    if [ ${mid_io_wait_time} -ge ${app_io_wait_time} ]; then
        echo "Test failed! ${mid_io_wait_time} >= ${app_io_wait_time}"
        ret=1
    else
        echo "Test passed! ${mid_io_wait_time} < ${app_io_wait_time}"
    fi

    # Remove large temp files
    rm -f "${mid_test_file}" "${app_test_file}"
    return $ret
}

test_blkio_weights_random_read() {
    local mid_test_file
    local app_test_file
    local pid
    local ret=0
    _setup_blkio

    echo "Test setup complete, beginning test..."
    ## Test ##
    # Do random read disk I/O
    _do_random_read "${mid_test_file}" &
    echo "# (putting dd process $! in ${MID_CG} cgroup)"
    echo $! > "${CGFS_PATH}/blkio/${MID_CG}/tasks"
    PROC_KILL_LIST="${PROC_KILL_LIST} $(cat "${CGFS_PATH}/blkio/${MID_CG}/tasks")"
    _do_random_read "${app_test_file}" &
    echo "# (putting dd process $! in ${APP_CG} cgroup)"
    echo $! > "${CGFS_PATH}/blkio/${APP_CG}/tasks"
    PROC_KILL_LIST="${PROC_KILL_LIST} $(cat "${CGFS_PATH}/blkio/${APP_CG}/tasks")"
    echo "# Waiting for random read processes..."
    wait

    head "${CGFS_PATH}/blkio/$APP_CG"/* 2>&1 | sed -e 's/^/# /'
    head "${CGFS_PATH}/blkio/$MID_CG"/* 2>&1 | sed -e 's/^/# /'

    echo "Test complete, verifying results..."
    ## Verify ##
    echo "# blkio.io_wait_time for $MID_CG cgroup:"
    sed -e 's/^/#   /' < ${CGFS_PATH}/blkio/$MID_CG/blkio.io_wait_time
    echo "# blkio.io_wait_time for $APP_CG cgroup:"
    sed -e 's/^/#   /' < ${CGFS_PATH}/blkio/$APP_CG/blkio.io_wait_time
    local mid_io_wait_time=$(grep ^Total ${CGFS_PATH}/blkio/$MID_CG/blkio.io_wait_time | cut -f2 -d\ )
    echo "# total wait time for $MID_CG cgroup: $mid_io_wait_time"
    local app_io_wait_time=$(grep ^Total ${CGFS_PATH}/blkio/$APP_CG/blkio.io_wait_time | cut -f2 -d\ )
    echo "# total wait time for $APP_CG cgroup: $app_io_wait_time"
    # Check that the applications CG got less I/O time than the middleware CG
    if [ ${mid_io_wait_time} -ge ${app_io_wait_time} ]; then
        echo "Test failed! ${mid_io_wait_time} >= ${app_io_wait_time}"
        ret=1
    else
        echo "Test passed! ${mid_io_wait_time} < ${app_io_wait_time}"
    fi

    # Remove large temp files
    rm -f "${mid_test_file}" "${app_test_file}"
    return $ret
}

test_cpu_shares() {
    local ret=0
    local cpu_cg="cpu,cpuacct"

    if [ -z "${CPUBURN}" ]; then
        whine "CPU Burn not found! Please install the package 'cpuburn'"
        return 1
    fi

    ## Setup ##
    _reset_cgroup ${cpu_cg}
    # Set the resource allocation proportion
    echo 10240 > "${CGFS_PATH}/$cpu_cg/${MID_CG}/cpu.shares"
    echo 1024 > "${CGFS_PATH}/$cpu_cg/${APP_CG}/cpu.shares"
    _drop_caches

    echo "Test setup complete, beginning test..."
    ## Test ##
    _burn_cpus &
    echo "# (putting CPU load process $! in ${MID_CG} cgroup)"
    echo $! > "${CGFS_PATH}/${cpu_cg}/${MID_CG}/tasks"
    _burn_cpus &
    echo "# (putting CPU load process $! in ${APP_CG} cgroup)"
    echo $! > "${CGFS_PATH}/${cpu_cg}/${APP_CG}/tasks"
    echo "# Waiting for CPU load processes..."
    wait

    head "${CGFS_PATH}/${cpu_cg}/${MID_CG}"/* 2>&1 | sed -e 's/^/# /'
    head "${CGFS_PATH}/${cpu_cg}/${APP_CG}"/* 2>&1 | sed -e 's/^/# /'

    echo "Test complete, verifying results..."
    ## Verify ##
    local mid_cpu_usage=$(cut -f2 -d\ < ${CGFS_PATH}/$cpu_cg/$MID_CG/cpuacct.usage)
    local app_cpu_usage=$(cut -f2 -d\ < ${CGFS_PATH}/$cpu_cg/$APP_CG/cpuacct.usage)

    if [ ${mid_cpu_usage} -le ${app_cpu_usage} ]; then
        echo "Test failed! ${mid_cpu_usage} < ${app_cpu_usage}"
        ret=1
    fi
    return $ret
}

test_memory_limits() {
    local ret=0
    local mid_cg_logfile="${WORKDIR}/${MID_CG}_memory_used"
    local app_cg_logfile="${WORKDIR}/${APP_CG}_memory_used"

    ## Setup ##
    _reset_cgroup memory
    echo "# Setting memory limit for ${MID_CG}: ${MID_HARD_MEM_LIMIT}..."
    echo ${MID_HARD_MEM_LIMIT} > "${CGFS_PATH}/memory/$MID_CG/memory.limit_in_bytes"
    echo "# Setting memory limit for ${APP_CG}: ${APP_HARD_MEM_LIMIT}..."
    echo ${APP_HARD_MEM_LIMIT} > "${CGFS_PATH}/memory/$APP_CG/memory.limit_in_bytes"
    _drop_caches

    echo "Test setup complete, beginning test..."
    ## Test ##
    # Read large amounts of data into memory
    _balloon_memory "${mid_cg_logfile}" &
    echo $! > "${CGFS_PATH}/memory/$MID_CG/tasks"
    PROC_KILL_LIST="${PROC_KILL_LIST} $(cat "${CGFS_PATH}/memory/${MID_CG}/tasks")"
    wait %1 || true
    sed -e "s/^/# $!: /" < "${mid_cg_logfile}"

    # Read large amounts of data into memory
    _balloon_memory "${app_cg_logfile}" &
    echo $! > "${CGFS_PATH}/memory/$APP_CG/tasks"
    PROC_KILL_LIST="${PROC_KILL_LIST} $(cat "${CGFS_PATH}/memory/${APP_CG}/tasks")"
    wait %1 || true
    sed -e "s/^/# $!: /" < "${app_cg_logfile}"

    head "${CGFS_PATH}/memory/${MID_CG}"/* 2>&1 | sed -e 's/^/# /'
    head "${CGFS_PATH}/memory/${APP_CG}"/* 2>&1 | sed -e 's/^/# /'

    echo "Test complete, verifying results..."
    ## Verify ##
    # Check that the max usage was the same as what was defined
    local mid_cg_usage=$(cat "${CGFS_PATH}/memory/$MID_CG/memory.max_usage_in_bytes")
    echo "# ${MID_CG} max_usage_in_bytes: ${mid_cg_usage}"
    if [ ${mid_cg_usage} -ne ${MID_HARD_MEM_LIMIT} ]; then
        echo "${MID_CG} memory usage test failed! ${mid_cg_usage} != ${MID_HARD_MEM_LIMIT}"
        ret=1
    fi

    # Check that the max usage was the same as what was defined
    local app_cg_usage=$(cat "${CGFS_PATH}/memory/$APP_CG/memory.max_usage_in_bytes")
    echo "# ${APP_CG} max_usage_in_bytes: ${app_cg_usage}"
    if [ ${app_cg_usage} -ne ${APP_HARD_MEM_LIMIT} ]; then
        echo "${APP_CG} memory usage test failed! ${app_cg_usage} != ${APP_HARD_MEM_LIMIT}"
        ret=1
    fi

    # Check that the max usage for the middleware CG was less than for the applications CG
    echo "# syncing..."
    sync
    mid_mem_used=$(cat "${mid_cg_logfile}")
    echo "# ${MID_CG} memory used, excluding overhead: ${mid_mem_used}"
    app_mem_used=$(cat "${app_cg_logfile}")
    echo "# ${APP_CG} memory used, excluding overhead: ${app_mem_used}"
    if [ ${mid_mem_used} -gt ${app_mem_used} ]; then
        echo "Memory used comparison test failed! ${mid_mem_used} > ${app_mem_used}"
        ret=1
    fi

    return $ret
}

# Set a threshold, and check if we get notified when we reach it
test_memory_threshold_notification() {
    local ret=0
    local cg="${APP_CG}"
    local notifier_pid
    local balloon_pid
    local notifier_status
    local notifier_status_log="${WORKDIR}/cgroup-memory-threshold-notifier.log"
    local balloon_logfile="${WORKDIR}/${cg}_memory_used.log"

    ## Setup ##
    _reset_cgroup memory
    _drop_caches

    ## Test ##
    echo "Running notifier..."
    _run_memory_threshold_notifier "${notifier_status_log}" "${cg}" &
    notifier_pid=$!

    echo "Running function to eat up memory..."
    MAX_MEMORY_LIMIT=$((20*1024*1024)) \
        _balloon_memory "${balloon_logfile}" &
    balloon_pid=$!
    echo ${balloon_pid} > "${CGFS_PATH}/memory/${cg}/tasks"
    PROC_KILL_LIST="${PROC_KILL_LIST} ${balloon_pid}"
    echo "Waiting for memory eating to finish..."
    if ! wait ${balloon_pid}; then
        ret=1
    fi
    sed -e "s/^/# ${balloon_pid}: /" < "${balloon_logfile}"

    echo "Test complete, verifying results..."
    ## Verify ##
    sync
    notifier_status=$(tail -n 1 < "${notifier_status_log}")
    if [ ${notifier_status} != 0 ]; then
        echo "Test failed! Notifier exited with status '${notifier_status}'"
        ret=1
    fi
    return $ret
}

test_network_cgroup_priority_classification() {
    local mid_test_file
    local mid_time
    local mid_flow
    local mid_pid
    local app_test_file
    local app_time
    local app_flow
    local app_pid
    local i
    local ret=0
    local cg_type="net_prio"
    # Timeout after 5 min, and always print the total time taken in the end
    local curl="${CURL} -m $((5*60)) -w %{time_total} -o /dev/null"

    ## Setup ##
    _reset_cgroup net_prio
    _setup_netprio
    _setup_qdiscs
    _write_random_files 5

    echo "Test setup complete, beginning test..."
    ## Test ##
    echo "# Configuring two http servers..."
    _setup_http_server 9002 "${cg_type}/${MID_CG}"
    _setup_http_server 9001 "${cg_type}/${APP_CG}"
    # Wait for servers to start up
    echo "# Waiting 5 seconds for servers to start..."
    _sleep 5
    echo "# Fetching ${mid_test_file} from ${MID_CG}..."
    ${curl} "http://localhost:9002/${mid_test_file##*/}" > "${WORKDIR}/mid_time.out" 2>"${WORKDIR}/network-cgroup-curl-9002.log" &
    mid_pid=$!
    echo "# (curl pid: $mid_pid)"
    echo "# Fetching ${app_test_file} from ${APP_CG}..."
    ${curl} "http://localhost:9001/${app_test_file##*/}" > "${WORKDIR}/app_time.out" 2>"${WORKDIR}/network-cgroup-curl-9001.log" &
    app_pid=$!
    echo "# (curl pid: $app_pid)"
    PROC_KILL_LIST="${PROC_KILL_LIST} $mid_pid $app_pid"
    wait $mid_pid || ret=$?
    wait $app_pid || ret=$?
    sed -e "s,^,# ${MID_CG} curl stdout: ," < "${WORKDIR}/mid_time.out"
    sed -e "s,^,# ${APP_CG} curl stdout: ," < "${WORKDIR}/app_time.out"
    sed -e "s,^,# ${MID_CG} curl stderr: ," < "${WORKDIR}/network-cgroup-curl-9002.log"
    sed -e "s,^,# ${APP_CG} curl stderr: ," < "${WORKDIR}/network-cgroup-curl-9001.log"
    sed -e "s,^,# ${MID_CG} server stderr: ," < "${WORKDIR}/network-cgroup-http-server-9002.log"
    sed -e "s,^,# ${APP_CG} server stderr: ," < "${WORKDIR}/network-cgroup-http-server-9001.log"
    # We measure the times now so that we can add a time-taken comparison test 
    # for priorities later, when we have a configuration
    mid_time=$(cat "${WORKDIR}/mid_time.out")
    app_time=$(cat "${WORKDIR}/app_time.out")

    echo "Test complete, verifying results..."
    ## Verify ##
    tc -s qdisc ls dev lo | sed -e "s/^/# tc -s qdisc ls dev lo/"
    high_flow=$(tc -s qdisc ls dev lo | grep -A1 "parent 10:1" | sed -n 's/ Sent \([0-9]\+\).*/\1/p')
    echo "High priority flow 10:1: sent ${high_flow}"
    mid_flow=$(tc -s qdisc ls dev lo | grep -A1 "parent 10:2" | sed -n 's/ Sent \([0-9]\+\).*/\1/p')
    echo "Mid priority flow 10:2: sent ${mid_flow}"
    low_flow=$(tc -s qdisc ls dev lo | grep -A1 "parent 10:3" | sed -n 's/ Sent \([0-9]\+\).*/\1/p')
    echo "Low priority flow 10:3: sent ${low_flow}"
    if [ ${high_flow} -eq 0 ] || [ ${mid_flow} -eq 0 ] || [ ${low_flow} -ne 0 ] || [ ${high_flow} -gt ${mid_flow} ]; then
        echo "Test failed! high_flow:${high_flow} mid_flow:${mid_flow} low_flow:${low_flow}"
        ret=1
    else
        echo "Test passed! high_flow:${high_flow} mid_flow:${mid_flow} low_flow:${low_flow}"
    fi

    ## Cleanup ##
    _wipe_qdiscs
    for i in ${HTTP_SERVER_PIDS}; do
        kill -9 ${i}
    done
    rm -f "${mid_test_file}" "${app_test_file}"
    return $ret
}

kill_procs_and_maybe_fail () {
    s=$?
    _run_cmd _kill_procs
    [ $s -eq 0 ] || test_failure
}
trap "kill_procs_and_maybe_fail" EXIT

if [ -n "$1" ]; then
    e=0
    $1 || e=$?
    case "$e" in
        (0)
            echo "$1 passed (exit $e)"
            ;;
        (77)
            echo "$1 skipped (exit $e)"
            ;;
        (*)
            echo "$1 failed (exit $e)"
            ;;
    esac
    exit $e
fi

src_test_pass <<-EOF
test_cpu_shares
test_memory_limits
test_memory_threshold_notification
test_blkio_weights_random_read
test_blkio_weights_sequential_read
test_network_cgroup_priority_classification
EOF

test_success
